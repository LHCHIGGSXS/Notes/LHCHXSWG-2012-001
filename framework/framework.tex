\section{Interim framework for the search of deviations}
The idea behind this framework is that all deviations from the SM
are computed assuming that there is only one underlying state at $\sim125\UGeV$.
It is assumed that this state is a Higgs boson,
i.e.\ the excitation of a field whose vacuum expectation value (VEV) breaks electroweak symmetry,
and that it is SM-like,
in the sense that the experimental results so far are compatible
with the interpretation of the state in terms of the SM Higgs boson.
No specific assumptions are made on any additional states of new physics
(and their decoupling properties)
that could influence the phenomenology of the $125\UGeV$ state,
such as additional Higgs bosons (which could be heavier but also lighter than $125\UGeV$),
additional scalars that do not develop a VEV,
and new fermions and/or gauge bosons that could interact with the state at $125\UGeV$, 
giving rise, for instance, to an invisible decay mode.

The purpose of this framework is to
either confirm that the light, narrow, resonance indeed matches the properties of the SM Higgs,
or to establish a deviation from the SM behaviour, which would rule out the SM if sufficiently
significant.
In the latter case the next goal in the quest to identify the nature of
electroweak symmetry breaking (EWSB) would obviously
be to test the compatibility of the observed patterns with alternative frameworks of EWSB. 

% The idea behind our framework is that all deviations from SM are computed assuming that there 
% is only one underlying state at $\sim125\UGeV$ which we assume to be a SM-like Higgs boson,
% i.e.\ the excitation of a field whose vacuum expectation value (VEV) breaks electroweak symmetry. 
% We make no specific assumption on the existence and nature of other heavy (scalar or not) degrees 
% of freedom which can influence the SM-like Higgs boson couplings to all SM particles; furthermore
% no assumption is made on their decoupling as their masses increase or on the mass-mixing with
% the SM-like Higgs boson. 
% 
% The heavy scalar degrees of freedom are Higgs-partners: they are not in the SM, but may
% have a rich spectrum of non-Higgs states (they do not necessarily develop a VEV). 
% Their spectrum and couplings to fermions and vector bosons will be strongly model dependent and
% our frameworks are intended to be part of a larger program (the so-called inverse problem):
% if LHC finds evidence for physics beyond the SM, how can one determine the underlying theory?
% Therefore, our framework is designed for proving that the light, narrow, resonance matches 
% the SM Higgs properties, or to establish that deviations from the SM behaviour are consistent 
% with some other EWSB framework.

In investigating the experimental information that can be obtained on the coupling properties
of the new state near $125\UGeV$ from the LHC data to be collected in 2012
the following assumptions are made\footnote{The experiments are encouraged to test the assumptions of the framework, but that lies outside the scope of this document.}:
\begin{itemize}
 \item The signals observed in the different search channels
originate from a single narrow resonance with a mass near $125\UGeV$. 
The case of several, possibly overlapping, resonances in this mass 
region is not considered.
 \item The width of the assumed Higgs boson near $125\UGeV$ is neglected, 
i.e.\ the zero-width approximation for this state is used.
Hence the
%product $\sigma\times BR(\mathit{ii}\to\PH\to\mathit{ff})$
signal cross section
can be decomposed in the following way for all channels:
 \begin{equation}
\left(\sigma\cdot\text{BR}\right)(\mathit{ii}\to\PH\to\mathit{ff}) = \frac{\sigma_{\mathit{ii}}\cdot\Gamma_{\mathit{ff}}}{\Gamma_{\PH}}
 \end{equation}
 where $\sigma_{\mathit{ii}}$ is the production cross section through the initial state $\mathit{ii}$, $\Gamma_{\mathit{ff}}$ the partial decay width into the final state $\mathit{ff}$ and $\Gamma_{\PH}$ the total width of the Higgs boson.
\end{itemize}

Within the context of these assumptions, in the following
a simplified framework for investigating the
experimental information that can be obtained on the coupling properties
of the new state is outlined.
In general, the couplings of the assumed Higgs state near $125\UGeV$ are
``pseudo-observables'', i.e.\ they cannot be directly measured. 
This means that a certain ``unfolding procedure'' is
necessary to extract information on the couplings from the
measured quantities like cross sections times branching ratios (for
specific experimental cuts and acceptances).
This gives rise to a certain model dependence of the extracted information.
Different options can be pursued in this context.
One possibility is to confront a specific model with the experimental data.
This has the advantage that all available higher-order corrections within this model can
consistently be included and also other experimental
constraints (for instance from direct searches or from electroweak
precision data) can be taken into account.
However, the results obtained in this case are restricted to the interpretation within
that particular model.
Another possibility is to use a general
parametrization of the couplings of the new state without referring to any
particular model.
While this approach is clearly less model-dependent,
the relation between the extracted coupling parameters and the couplings of actual models, 
for instance the SM or its minimal
supersymmetric extension (MSSM), is in general non-trivial, so that the
theoretical interpretation of the extracted information can
be difficult. 
It should be mentioned that the results for the 
signal strengths of individual search channels that have been made
public by ATLAS and CMS, while referring just to a particular search
channel rather than to the full information available from the Higgs
searches, are nevertheless very valuable for testing the predictions
of possible models of physics beyond the SM.

In the SM, once the numerical value of the Higgs
mass is specified, all the couplings of the Higgs boson to fermions,
bosons and to itself are specified within the model.
It is therefore in
general not possible to perform a fit to experimental data within the
context of the SM where Higgs couplings are treated as free parameters. 
While it is possible to test the overall compatibility of the SM with
the data, it is not possible to extract information about 
deviations of the measured couplings with respect to their SM values. 

A theoretically well-defined framework for probing small deviations from the
SM predictions --- or the predictions of another reference model --- is to use
the state-of-the-art predictions in this model (including all available
higher-order corrections) and to supplement them with the contributions
of additional terms in the Lagrangian,
which are usually called ``anomalous couplings''.
In such an approach and in general, not only the coupling strength, i.e.\ the
absolute value of a given coupling, will be modified, but also the
tensor structure of the coupling.
For instance, the $\PH\PWp\PWm$ LO coupling 
in the SM is proportional to the metric tensor $g^{\mu\nu}$, while
anomalous couplings will generally also give rise to other tensor
structures, however required to be compatible with the SU(2)$\times$U(1)
symmetry and the corresponding Ward-Slavnov-Taylor identities.
As a consequence, kinematic distributions will in general
be modified when compared to the SM case. 

Since the reinterpretation of searches that have been performed within
the context of the SM is difficult if effects that change kinematic
distributions are taken into account and since not all the necessary
tools to perform this kind of analysis are available yet, the
following additional assumption is made in this simplified framework:

\begin{itemize}

\item
Only modifications of couplings strengths, i.e.\ of absolute values of
couplings, are taken into account, while the tensor structure of the
couplings is assumed to be the same as in the SM prediction. This means in
particular that the observed state is assumed to be a CP-even scalar.

\end{itemize}

\subsection{Definition of coupling scale factors}
\label{sec:scale_factor_def}

In order to take into account the currently best available SM
predictions for Higgs cross sections, which include
higher-order QCD and EW corrections~\cite{Dittmaier:2011ti,Dittmaier:2012vm,HiggsWG},
while at the same time introducing
possible deviations from the SM values of the couplings, the 
predicted SM Higgs cross sections and partial
decay widths are dressed with scale factors $\Cc_i$. 
The scale factors $\Cc_i$ are defined in such a way that the cross sections
$\sigma_{ii}$ or the partial decay widths $\Gamma_{ii}$ associated with the SM particle $i$ scale
with the factor $\Cc_i^2$ when compared to the corresponding SM prediction.
Table~\ref{tab:LO_coupling_relatios} lists all relevant cases.
Taking the process $\Pg\Pg\to\PH\to\PGg\PGg$ as an example, one would use as cross section:
\begin{eqnarray}
\left(\sigma\cdot\text{BR}\right)(\Pg\Pg\to\PH\to\PGg\PGg) &=& \sigma_\text{SM}(\Pg\Pg\to\PH) \cdot \text{BR}_\text{SM}(\PH\to\PGg\PGg)\,\cdot \frac{\Cc_{\Pg}^2 \cdot \Cc_{\PGg}^2}{\Cc_{\PH}^2}
\end{eqnarray}
where the values and uncertainties for both $\sigma_\text{SM}(\Pg\Pg\to\PH)$
and $\text{BR}_\text{SM}(\PH\to\PGg\PGg)$ are taken from Ref.~\cite{HiggsWG} for a given Higgs mass hypothesis.



\begin{table}
\centering
\begin{tabular}{@{}p{0.43\linewidth}|p{0.55\linewidth}@{}}
\hline
\begin{eqnarray}
  \omit\rlap{\text{Production modes}}\nonumber\\
  \frac{\sigma_{\MyggH}}{\sigma_{\MyggH}^\text{SM}}               & = & \left\{ \begin{array}{l} \Cc_{\Pg}^2(\Cc_{\PQb},\Cc_{\PQt},\mH) \\ \Cc_{\Pg}^2 \end{array} \right. \\
  \frac{\sigma_{\text{VBF}}}{\sigma_{\text{VBF}}^\text{SM}} & = & \Cc_\text{VBF}^2(\Cc_{\PW},\Cc_{\PZ},\mH)\\
  \frac{\sigma_{\PW\PH}}{\sigma_{\PW\PH}^\text{SM}}                 & = & \Cc_{\PW}^2\\
  \frac{\sigma_{\PZ\PH}}{\sigma_{\PZ\PH}^\text{SM}}                 & = & \Cc_{\PZ}^2\\
  \frac{\sigma_{\MyttH}}{\sigma_{\MyttH}^\text{SM}}    & = & \Cc_{\PQt}^2
\end{eqnarray}
&
% \begin{minipage}[t]{\linewidth}
% Detectable decay modes
\begin{eqnarray}
%   \hline
  \omit\rlap{\text{Detectable decay modes}}\nonumber\\
  \frac{\Gamma_{\PW\PW^{(*)}}}{\Gamma_{\PW\PW^{(*)}}^\text{SM}}           &=& \Cc_{\PW}^2\\
  \frac{\Gamma_{\PZ\PZ^{(*)}}}{\Gamma_{\PZ\PZ^{(*)}}^\text{SM}}           &=& \Cc_{\PZ}^2\\
  \frac{\Gamma_{\PQb\PAQb}}{\Gamma_{\PQb\PAQb}^\text{SM}}           &=& \Cc_{\PQb}^2\\
  \frac{\Gamma_{\PGtm\PGtp}}{\Gamma_{\PGtm\PGtp}^\text{SM}}     &=& \Cc_{\PGt}^2\\
  \frac{\Gamma_{\PGg\PGg}}{\Gamma_{\PGg\PGg}^\text{SM}} &=& \left\{ \begin{array}{l} \Cc_{\PGg}^2(\Cc_{\PQb},\Cc_{\PQt},\Cc_{\PGt},\Cc_{\PW},\mH) \\ \Cc_{\PGg}^2 \end{array} \right.\\
  \frac{\Gamma_{\PZ\PGg}}{\Gamma_{\PZ\PGg}^\text{SM}} &=& \left\{ \begin{array}{l} \Cc_{(\PZ\PGg)}^2(\Cc_{\PQb},\Cc_{\PQt},\Cc_{\PGt},\Cc_{\PW},\mH) \\ \Cc_{(\PZ\PGg)}^2 \end{array} \right.\\
% \end{eqnarray}
% 
% Undetectable decay modes
% \begin{eqnarray}  
%   \hline
  \nonumber\\
  \omit\rlap{\text{Currently undetectable decay modes}}\nonumber\\
  \frac{\Gamma_{\PQt\PAQt}}{\Gamma_{\PQt\PAQt}^\text{SM}}           &=& \Cc_{\PQt}^2\\
  \frac{\Gamma_{\Pg\Pg}}{\Gamma_{\Pg\Pg}^\text{SM}}           &:& \text{see Section~\ref{sec:C_g}}\nonumber\\
  \frac{\Gamma_{\PQc\PAQc}}{\Gamma_{\PQc\PAQc}^\text{SM}}           &=& \Cc_{\PQt}^2\\
  \frac{\Gamma_{\PQs\PAQs}}{\Gamma_{\PQs\PAQs}^\text{SM}}           &=& \Cc_{\PQb}^2\\
  \frac{\Gamma_{\PGmm\PGmp}}{\Gamma_{\PGmm\PGmp}^\text{SM}}       &=& \Cc_{\PGt}^2\\
% \end{eqnarray}
% 
% Total width
% \begin{eqnarray}    
%   \hline
  \nonumber\\
  \omit\rlap{\text{Total width}}\nonumber\\
  \frac{\Gamma_{\PH}}{\Gamma_{\PH}^\text{SM}}           &=& \left\{ \begin{array}{l} \Cc_{\PH}^2(\Cc_i,\mH) \\ \Cc_{\PH}^2 \end{array} \right.                    
\end{eqnarray}
% \end{minipage}
\\
\hline
\end{tabular}
\caption{LO coupling scale factor relations for Higgs boson cross sections and partial decay widths relative to the SM.
For a given $\mH$ hypothesis, the smallest set of degrees of freedom in this framework comprises
$\Cc_{\PW}$, $\Cc_{\PZ}$, $\Cc_{\PQb}$, $\Cc_{\PQt}$, and $\Cc_{\PGt}$.
For partial widths that are not detectable at the LHC, scaling is performed via proxies chosen among the detectable ones.  
Additionally, the loop-induced vertices can be treated as a function of other $\Cc_i$ or effectively,
through the $\Cc_{\Pg}$ and $\Cc_{\PGg}$ degrees of freedom which allow probing for BSM contributions in the loops.
Finally, to explore invisible or undetectable decays,
the scaling of the total width can also be taken as a separate degree of freedom, $\Cc_{\PH}$,
instead of being rescaled as a function, $\Cc_{\PH}^2(\Cc_i,\mH)$, of the other scale factors.}
\label{tab:LO_coupling_relatios}
\end{table}

By definition, the currently best available SM predictions
for all $\sigma\cdot\text{BR}$ are recovered when all $\Cc_i=1$.
In general, this means that for $\Cc_i\neq 1$ higher-order accuracy is lost.
Nonetheless, NLO QCD corrections essentially factorize with respect to coupling rescaling,
and are accounted for wherever possible.
This approach ensures that for a true SM Higgs boson no artifical deviations
(caused by ignored NLO corrections) are found from what is considered the SM Higgs boson hypothesis.
The functions
$\Cc_\text{VBF}^2(\Cc_{\PW},\Cc_{\PZ},\mH)$,
$\Cc_{\Pg}^2(\Cc_{\PQb},\Cc_{\PQt},\mH)$,
$\Cc_{\PGg}^2(\Cc_{\PQb},\Cc_{\PQt},\Cc_{\PGt},\Cc_{\PW},\mH)$ and
$\Cc_{\PH}^2(\Cc_i,\mH)$
 are used for cases where there is a non-trivial relationship between scale factors $\Cc_i$ and cross sections
 or (partial) decay widths, and are calculated to NLO QCD accuracy.
 The functions are defined in the following sections and 
 all required input parameters as well as example code can be found in~\Brefs{HiggsWG,LMwiki}.
As explained in Sec.~\ref{Subsub:int} below, the
notation in terms of the partial widths $\Gamma_{\PW\PW^{(*)}}$ and $\Gamma_{\PZ\PZ^{(*)}}$ in Table~\ref{tab:LO_coupling_relatios}
is meant for illustration only. In the experimental analysis the
4-fermion partial decay widths are taken into account.


\subsubsection{Scaling of the VBF cross section}
$\Cc_\text{VBF}^2$ refers to the functional dependence of the
VBF\footnote{Vector Boson Fusion is also called Weak Boson Fusion, as only the weak bosons $\PW$ and $\PZ$ contribute to the production.}
cross section on the scale factors $\Cc_{\PW}^2$ and $\Cc_{\PZ}^2$:
\begin{eqnarray}
\label{eq:RVBF}
 \Cc_\text{VBF}^2(\Cc_{\PW},\Cc_{\PZ},\mH) &=& \frac{\Cc_{\PW}^2 \cdot \sigma_{\PW F}(\mH) + \Cc_{\PZ}^2 \cdot \sigma_{\PZ F}(\mH)}{\sigma_{\PW F}(\mH)+\sigma_{\PZ F}(\mH)}
\end{eqnarray}
The $\PW$- and $\PZ$-fusion cross sections,
$\sigma_{\PW F}$ and $\sigma_{\PZ F}$,
are taken from \Brefs{Arnold:2008rz,webVBFNLO}.
The interference term is $< 0.1\%$ in the SM and hence ignored~\cite{Ciccolini:2007ec}.


\subsubsection{Scaling of the gluon fusion cross section and of the $\PH\to\Pg\Pg$ decay vertex}
\label{sec:C_g}
$\Cc_{\Pg}^2$ refers to the scale factor for the loop-induced production
cross section $\sigma_{\MyggH}$.
The decay width $\Gamma_{\Pg\Pg}$ is not observable at the LHC, however its contribution to the total width
is also considered.

\paragraph*{Gluon fusion cross-section scaling}
As NLO QCD corrections factorize with the scaling of the electroweak couplings with $\Cc_{\PQt}$ and $\Cc_{\PQb}$,
the function $\Cc_{\Pg}^2(\Cc_{\PQb}, \Cc_{\PQt},\mH)$ can be calculated in NLO QCD:
\begin{eqnarray}
\label{eq:CgNLOQCD}
 \Cc_{\Pg}^2(\Cc_{\PQb}, \Cc_{\PQt},\mH) &=& \frac{\Cc_{\PQt}^2\cdot\sigma_{\MyggH}^{\PQt\PQt}(\mH) +\Cc_{\PQb}^2\cdot\sigma_{\MyggH}^{\PQb\PQb}(\mH) +\Cc_{\PQt}\Cc_{\PQb}\cdot\sigma_{\MyggH}^{\PQt\PQb}(\mH)}{\sigma_{\MyggH}^{\PQt\PQt}(\mH)+\sigma_{\MyggH}^{\PQb\PQb}(\mH)+\sigma_{\MyggH}^{\PQt\PQb}(\mH)}
\end{eqnarray}

Here, $\sigma_{\MyggH}^{\PQt\PQt}$, $\sigma_{\MyggH}^{\PQb\PQb}$ and $\sigma_{\MyggH}^{\PQt\PQb}$
denote the square of the top-quark contribution, the square of the bottom-quark
contribution and the top-bottom interference, respectively.
The interference term ($\sigma_{\MyggH}^{\PQt\PQb}$) is negative for a light mass Higgs, $\mH < 200\UGeV$. 
Within the LHC Higgs Cross Section Working Group (for the evaluation of the
MSSM cross section) these contributions were evaluated, where for
$\sigma_{\MyggH}^{\PQb\PQb}$ and $\sigma_{\MyggH}^{\PQt\PQb}$ the full NLO QCD calculation included
in \HIGLU~\cite{Spira:1995mt} was used.
For $\sigma_{\MyggH}^{\PQt\PQt}$ the NLO QCD result of \HIGLU\ was supplemented with the
NNLO corrections in the heavy-top-quark limit as implemented in \GGHNNLO~\cite{Harlander:2002wh},
see~Ref.~\cite[Sec.~6.3]{Dittmaier:2011ti} for details.

\paragraph*{Partial width scaling}
In a similar way, NLO QCD corrections for the
$\PH\to \Pg\Pg$ partial width are implemented in \HDECAY~\cite{Spira:1996if,Djouadi:1997yw,hdecay2}.
This allows to treat the scale factor for $\Gamma_{\Pg\Pg}$
as a second order polynomial in $\Cc_{\PQb}$ and $\Cc_{\PQt}$:
\begin{eqnarray}
\label{eq:GammagNLOQCD}
\frac{\Gamma_{\Pg\Pg}}{\Gamma_{\Pg\Pg}^\text{SM}(\mH)} = \frac{\Cc_{\PQt}^2\cdot\Gamma_{\Pg\Pg}^{\PQt\PQt}(\mH) +\Cc_{\PQb}^2\cdot\Gamma_{\Pg\Pg}^{\PQb\PQb}(\mH) +\Cc_{\PQt}\Cc_{\PQb}\cdot\Gamma_{\Pg\Pg}^{\PQt\PQb}(\mH)}{\Gamma_{\Pg\Pg}^{\PQt\PQt}(\mH)+\Gamma_{\Pg\Pg}^{\PQb\PQb}(\mH)+\Gamma_{\Pg\Pg}^{\PQt\PQb}(\mH)}
\end{eqnarray}
The terms $\Gamma_{\Pg\Pg}^{\PQt\PQt}$, $\Gamma_{\Pg\Pg}^{\PQb\PQb}$ and $\Gamma_{\Pg\Pg}^{\PQt\PQb}$
are defined like the $\sigma_{\MyggH}$ terms in Eq.~(\ref{eq:CgNLOQCD}).
The $\Gamma_{\Pg\Pg}^{ii}$ correspond to the partial widths that are obtained for $\Cc_i=1$ and all other $\Cc_j=0, j\neq i$.
The cross-term $\Gamma_{\Pg\Pg}^{\PQt\PQb}$ can then be derived
by calculating the SM partial width by setting $\Cc_{\PQb}=\Cc_{\PQt}=1$ and
subtracting $\Gamma_{\Pg\Pg}^{\PQt\PQt}$ and $\Gamma_{\Pg\Pg}^{\PQb\PQb}$ from it. 

\paragraph*{Effective treatment}
In the general case, without the assumptions above, possible non-zero contributions from additional
particles in the loop have to be taken into account and $\Cc_{\Pg}^2$ is then treated as an
effective coupling scale factor parameter in the fit:
% \begin{eqnarray}
%  \frac{\sigma_{\MyggH}}{\sigma_{\MyggH}^\text{SM}} &=& \Cc_{\Pg}^2
% \end{eqnarray}
$\sigma_{\MyggH}/\sigma_{\MyggH}^\text{SM} = \Cc_{\Pg}^2$.
The effective scale factor for the partial gluon width $\Gamma_{\Pg\Pg}$ should behave in a very similar way,
so in this case the same effective scale factor $\Cc_{\Pg}$ is used:
% \begin{eqnarray}
%  \frac{\Gamma_{\Pg\Pg}}{\Gamma_{\Pg\Pg}^\text{SM}}  &=& \Cc_{\Pg}^2
% \end{eqnarray}
$\Gamma_{\Pg\Pg}/\Gamma_{\Pg\Pg}^\text{SM} = \Cc_{\Pg}^2$.
As the contribution of $\Gamma_{\Pg\Pg}$ to the total width is <10\% in the SM,
this assumption is believed to have no measurable impact.

\subsubsection{Scaling of the $\PH\to\PGg\PGg$ partial decay width}
\label{sec:C_gamma}
Like in the previous section,
$\Cc_{\PGg}^2$ refers to the scale factor for the loop-induced $\PH\to\PGg\PGg$ decay.
Also for the $\PH\to\PGg\PGg$ decay NLO QCD corrections exist and are implemented in \HDECAY.
This allows to treat the scale factor for the $\PGg\PGg$ partial width
as a second order polynomial in $\Cc_{\PQb}$, $\Cc_{\PQt}$, $\Cc_{\PGt}$, and $\Cc_{\PW}$:
\begin{eqnarray}
\label{eq:CgammaNLOQCD}
\Cc_{\PGg}^2(\Cc_{\PQb}, \Cc_{\PQt}, \Cc_{\PGt}, \Cc_{\PW}, \mH) &=& \frac{\sum_{i,j}\Cc_i \Cc_j\cdot\Gamma_{\PGg\PGg}^{i j}(\mH)}{\sum_{i,j}\Gamma_{\PGg\PGg}^{ij}(\mH)}
\end{eqnarray}
where the pairs $(i,j)$ are $\PQb\PQb,\PQt\PQt,\PGt\PGt,\PW\PW,\PQb\PQt,\PQb\PGt,\PQb\PW,\PQt\PGt,\PQt\PW,\PGt\PW$.
The $\Gamma_{\PGg\PGg}^{ii}$ correspond to the partial widths that are obtained for $\Cc_i=1$ and all other $\Cc_j=0, (j\neq i)$.
The cross-terms $\Gamma_{\PGg\PGg}^{ij}, (i\neq j)$ can then be derived by calculating the partial width by setting $\Cc_i=\Cc_j=1$
and all other $\Cc_l=0, (l\neq i,j)$, and subtracting $\Gamma_{\PGg\PGg}^{ii}$ and $\Gamma_{\PGg\PGg}^{jj}$ from them. 

\paragraph*{Effective treatment}
In the general case, without the assumption above, possible non-zero contributions from additional
particles in the loop have to be taken into account
and $\Cc_{\PGg}^2$ is then treated as an effective coupling parameter in the fit.

\subsubsection{Scaling of the $\PH\to \PZ\PGg$ decay vertex}
\label{sec:C_Zgamma}
Like in the previous sections, $\Cc_{(\PZ\PGg)}^2$ refers to
the scale factor for the loop-induced $\PH\to \PZ\PGg$ decay.
This allows to treat the scale factor for the $\PZ\PGg$ partial width as a
second order polynomial in $\Cc_{\PQb}$, $\Cc_{\PQt}$, $\Cc_{\PGt}$, and $\Cc_{\PW}$:
\begin{eqnarray}
\label{eq:CZgammaNLOQCD}
\Cc_{(\PZ\PGg)}^2(\Cc_{\PQb}, \Cc_{\PQt}, \Cc_{\PGt}, \Cc_{\PW}, \mH) &=& \frac{\sum_{i,j}\Cc_i \Cc_j\cdot\Gamma_{\PZ\PGg}^{i j}(\mH)}{\sum_{i,j}\Gamma_{\PZ\PGg}^{i j}(\mH)}
\end{eqnarray}
where the pairs $(i,j)$ are $\PQb\PQb,\PQt\PQt,\PGt\PGt,\PW\PW,\PQb\PQt,\PQb\PGt,\PQb\PW,\PQt\PGt,\PQt\PW,\PGt\PW$.
The $\Gamma_{\PZ\PGg}^{ij}$ are calculated in the same way as for Eq.~(\ref{eq:CgammaNLOQCD}).
NLO QCD corrections have been computed and found to be very small \cite{Spira:1991tj}, and thus ignored here.

\paragraph*{Effective treatment}
In the general case, without the assumption above, possible non-zero contributions from additional
particles in the loop have to be taken into account
and $\Cc_{(\PZ\PGg)}^2$ is then treated as an effective coupling parameter in the fit.


\subsubsection{Scaling of the total width}
The total width $\Gamma_{\PH}$ is the sum of all Higgs partial decay widths.
Under the assumption that no additional BSM Higgs decay modes
(into either invisible or undetectable final states)
contribute to the total width,
$\Gamma_{\PH}$ is expressed as the sum of the scaled partial Higgs decay widths to SM particles,
which combine to a total scale factor $\Cc_{\PH}^2$ compared to the SM total width $\Gamma_{\PH}^\text{SM}$:
\begin{eqnarray}
  \label{eq:CH2_def}
  \Cc_{\PH}^2(\Cc_i,\mH) &=& \sum\limits_{\begin{array}{r}j=\PW\PW^{(*)},\PZ\PZ^{(*)},\PQb\PAQb,\PGtm\PGtp,\\\PGg\PGg,\PZ\PGg,\Pg\Pg,\PQt\PAQt,\PQc\PAQc,\PQs\PAQs,\PGmm\PGmp\end{array}} \frac{ \Gamma_j(\Cc_i,\mH)}{\Gamma_{\PH}^\text{SM}(\mH)}
\end{eqnarray}

\paragraph*{Effective treatment}
In the general case, additional Higgs decay modes to BSM particles cannot be excluded
and the total width scale factor $\Cc_{\PH}^2$ is treated as free parameter.

The total width $\Gamma_{\PH}$ for a light Higgs with $\mH\sim 125\UGeV$
is not expected to be directly observable at the LHC,
as the SM expectation is $\Gamma_{\PH}\sim 4\UMeV$,
several orders of magnitude smaller than
the experimental mass resolution.
There is no indication from the results observed so far that 
the natural width is broadened by new physics effects 
to such an extent that it could be directly observable.
Furthermore, as all LHC Higgs channels rely on the identification of Higgs decay products,
there is no way of measuring the total Higgs width indirectly within a coupling fit without using assumptions. 
This can be illustrated by assuming that all cross sections and partial widths are increased by a common factor
$\Cc_i^2=r>1$.
If simultaneously the Higgs total width is increased by the square of the same factor $\Cc_{\PH}^2=r^2$
(for example by postulating some BSM decay mode) the experimental visible signatures
in all Higgs channels would be indistinguishable from the SM.

Hence without further assumptions only ratios of scale factors $\Cc_i$ can be measured at the LHC,
where at least one of the ratios needs to include the total width scale factor $\Cc_{\PH}^2$.
Such a definition of ratios absorbs two degrees of freedom
(e.g.\ a common scale factor to all couplings and a scale factor to the total width)
into one ratio that can be measured at the LHC.
In order to go beyond the measurement of ratios of coupling scale factors to the
determination of absolute coupling scale factors $\Cc_i$ additional
assumptions are necessary to remove one degree of freedom.
Possible assumptions are:
\begin{itemize}
 \item No new physics in Higgs decay modes (Eq.~(\ref{eq:CH2_def})).
 \item $\Cc_{\PW}\le 1$, $\Cc_{\PZ}\le 1$. If one combines this assumption with the fact that all Higgs partial decay widths are
 positive definite and the total width is bigger than the sum of all (known) partial decay widths,
 this is sufficient to give a lower and upper bound on all $\Cc_i$ and also
 determine a possible branching ratio $\BRinv$ into final states invisible or undetectable at the LHC.
 This is best illustrated with the $\PV\PH(\PH\to\PV\PV)$ process: 
 \begin{eqnarray}
  \sigma_{\PV\PH}\cdot\text{BR}(\PH\to \PV\PV) &=& \frac{\Cc_{\PV}^2 \cdot \sigma_{\PV\PH}^\text{SM} \,\,\cdot\,\, \Cc_{\PV}^2 \cdot \Gamma^\text{SM}_{\PV}}{\Gamma_{\PH}}\nonumber\\
  \text{and\hspace{4cm}}\Gamma_{\PH} &>& \Cc_{\PV}^2 \cdot \Gamma_{\PV}^\text{SM}\label{eq:GammaH_sum_inequation}\\
  \text{give combined:\hspace{1cm}}\sigma_{\PV\PH}\cdot\text{BR}(\PH\to VV)&<&\frac{\Cc_{\PV}^2 \cdot \sigma_{\PV\PH}^\text{SM} \,\,\cdot\,\, \Cc_{\PV}^2 \cdot \Gamma^\text{SM}_{\PV}}{\Cc_{\PV}^2 \cdot \Gamma_{\PV}^\text{SM}}\nonumber\\
  \Longrightarrow\text{\hspace{4cm}}\Cc_{\PV}^2&>&\frac{\sigma_{\PV\PH}\cdot\text{BR}(\PH\to \PV\PV)}{\sigma_{\PV\PH}^\text{SM}}
 \end{eqnarray}
 If more final states are included in Eq.~(\ref{eq:GammaH_sum_inequation}), the lower bounds become tighter
 and together with the upper limit assumptions on $\Cc_{\PW}$ and $\Cc_{\PZ}$, absolute measurements are possible.
 However, uncertainties on all $\Cc_i$ can be very large depending
 on the accuracy of the $\PQb\PAQb$ decay channels that dominate the uncertainty
 of the total width sum.
\end{itemize}

In the following benchmark parametrizations always two versions are given:
one without assumptions on the total width
and one assuming no beyond SM Higgs decay modes.

\subsection{Further assumptions}

\subsubsection{Theoretical uncertainties}
The quantitative impact of theory uncertainties in the Higgs production cross sections and decay rates
is discussed in detail in \Bref{Dittmaier:2011ti}.

Such uncertainties will directly affect the determination of the scale factors.
When one or more of the scaling factors differ from 1, the uncertainty from missing higher-order
contributions will in general be larger than what was estimated in~\Bref{Dittmaier:2011ti}.
 
In practice, the cross section predictions with their uncertainties as tabulated in~\Bref{Dittmaier:2011ti}
are used as such so that for $\Cc_i=1$ the recommended SM treatment is recovered.
Without a consistent electroweak NLO calculation for deviations from the SM,
electroweak corrections and their uncertainties for the SM prediction
($\sim 5\%$ in gluon fusion production and $\sim 2\%$ in the di-photon decay)
are naively scaled together.
In the absence of explicit calculations
this is the currently best available approach
in a search for deviations from the SM Higgs prediction.

\subsubsection{Limit of the zero-width approximation}
Concerning the zero-width approximation (ZWA), it should be noted that in the 
mass range of the narrow resonance the width of the Higgs boson of the
Standard Model (SM) is more than four orders of magnitude smaller than 
its mass.
Thus, the zero-width approximation is in principle expected to
be an excellent approximation not only for a SM-like Higgs boson below $\sim 150\UGeV$ 
but also for a wide range of BSM scenarios which are
compatible with the present data. 
However, it has been shown in \Bref{Kauer:2012hd}
that this is not always the case even in the SM.
The inclusion of off-shell contributions is essential 
to obtain an accurate Higgs signal normalization at the $1\%$ precision level. 
For $\Pg\Pg\ (\to \PH) \to \PV\PV$, $\PV= \PW,\PZ$, 
${\cal O}(10\%)$ corrections occur due to an enhanced Higgs signal 
in the region $M_{\PV\PV} > 2\,M_{\PV}$, where also sizeable 
Higgs-continuum interference occurs.
However, with the accuracy anticipated to be reached in
the 2012 data these effects play a minor role.

\subsubsection{Signal interference effects}
\label{Subsub:int}
% A source of uncertainty is related to interference effects in $\PH \to 4\,$fermion
% decay. We refer to Chapter~2 of \Bref{Dittmaier:2012vm}, where it is shown that the ratio 
% of the ZWA
% %--
% \begin{equation}
% \mbox{BR}(\PH \to \PV \PV)\,\times\,\mbox{BR}^2(V \to {\overline f} f)
% \end{equation}
% %--
% over the complete result~\cite{Prophecy4f,Bredenstein:2006rh,Bredenstein:2006ha} for $\PH \to \Pep\Pem\Pep\Pem$ or 
% $\Pep\Pem\PGmp\PGmm$ is large, due to the interference (below $\PW\PW, \PZ\PZ$ thresholds),
% about $11\%$ difference.
% 
% The experimental analyses take into account the full NLO 4~fermion partial decay width.
% The partial width of the 4~lepton final state
% (usually referred to as $\PH\to \PZ\PZ^{(*)}\to 4l$ or $\PH\to \PZ\PZ^{(*)}\to 2l2j$, depending on decay mode) is scaled with $\Cc_{\PZ}^2$.
% The partial width of the low mass 2~lepton, 2~neutrino final state
% (usually referred to as $\PH\to \PW\PW^{(*)}\to l\nu\,l\nu$, although some interference with $\PH\to \PZ^{(*)}\PZ\to ll\,\nu\nu$ exists and is taken into account) is scaled with $\Cc_{\PW}^2$.
A possible source of uncertainty is related to interference effects
in $\PH \to 4\,$fermion decay.
For a light Higgs boson the decay width
into 4 fermions should always be calculated from the complete matrix
elements and not from the approximation
%--
\begin{equation}\label{eq:4fapprox}
\text{BR}(\PH \to \PV \PV)\,\cdot\,\text{BR}^2(\PV \to \Pf\PAf)
\end{equation}
%--
This approximation, based on the ZWA for the gauge boson $\PV$, neglects both off-shell
effects and interference between diagrams where the intermediate gauge
bosons couple to different pairs of final-state fermions.
As shown in Chapter~2 of \Bref{Dittmaier:2012vm}, the interference
effects not included in Eq.~(\ref{eq:4fapprox}) amount to 10\% for the
decay $\PH \to \Pep\Pem\Pep\Pem$ for a $125\UGeV$ Higgs.
Similar interference effects of the order of 5\% are found for the
$\Pep\PGne\Pem\PAGne$ and $\PQq\PAQq\PQq\PAQq$ final states.

The experimental analyses take into account the full NLO 4-fermion
partial decay width~\cite{Prophecy4f,Bredenstein:2006rh,Bredenstein:2006ha}.
The partial width of the 4-lepton final state
(usually described as $\PH\to \PZ\PZ^{(*)}\to 4\Pl$) is scaled with $\Cc_{\PZ}^2$.
Similary, the partial width of the 2-lepton, 2-jet final state
(usually described as $\PH\to\PZ\PZ^{(*)}\to 2\Pl 2\PQq$) is scaled with $\Cc_{\PZ}^2$.
The partial width of the low mass
2-lepton, 2-neutrino final state (usually described as $\PH\to \PW\PW^{(*)}\to
\Pl\PGn\,\Pl\PGn$, although a contribution of $\PH\to \PZ^{(*)}\PZ\to \Pl\Pl\,\PGn\PGn$
exists and is taken into account) is scaled with $\Cc_{\PW}^2$. 

\subsubsection{Treatment of $\Gamma_{\PQc\PAQc}$, $\Gamma_{\PQs\PAQs}$, $\Gamma_{\PGmm\PGmp}$ and light fermion contributions to loop-induced processes}
When calculating $\Cc_{\PH}^2(\Cc_i,\mH)$ in a benchmark parametrization,
the final states $\PQc\PAQc$, $\PQs\PAQs$ and $\PGmm\PGmp$
(currently unobservable at the LHC) are tied to $\Cc_i$ scale factors which can
be determined from the data.
Based on flavour symmetry considerations, the following choices are made:
\begin{eqnarray}
   \frac{\Gamma_{\PQc\PAQc}}{\Gamma_{\PQc\PAQc}^\text{SM}(\mH)}        &=& \Cc_{\PQc}^2   = \Cc_{\PQt}^2\\
   \frac{\Gamma_{\PQs\PAQs}}{\Gamma_{\PQs\PAQs}^\text{SM}(\mH)}        &=& \Cc_{\PQs}^2   = \Cc_{\PQb}^2\\
   \frac{\Gamma_{\PGmm\PGmp}}{\Gamma_{\PGmm\PGmp}^\text{SM}(\mH)} &=& \Cc_{\PGm}^2 = \Cc_{\PGt}^2
\end{eqnarray} 
Following the rationale of~Ref.~\cite[Sec. 9]{Dittmaier:2011ti},
the widths of $\Pem\Pep$, $\PQu\PAQu$, $\PQd\PAQd$ and neutrino final states are neglected.

Through interference terms,
these light fermions also contribute to the loop-induced $\Pg\Pg\to\PH$ and $\PH\to\Pg\Pg,\PGg\PGg,\PZ\PGg$ vertices.
In these cases, the assumptions $\Cc_{\PQc}=\Cc_{\PQt}$, $\Cc_{\PQs}=\Cc_{\PQb}$ and $\Cc_{\PGm}=\Cc_{\PGt}$ are made.

\subsubsection{Approximation in associated $\PZ\PH$ production}
When scaling the associated $\PZ\PH$ production mode,
the contribution from $\Pg\Pg\to \PZ\PH$ through a top-quark loop is neglected.
This is estimated to be around 5\%
of the total associated $\PZ\PH$ production cross section~\cite[Sec.~4.3]{Dittmaier:2011ti}.
