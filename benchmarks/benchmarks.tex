\section{Benchmark parametrizations}

In putting forward a set of benchmark parametrizations based on the framework
described in the previous section several considerations were taken into
account.
%
One concern is the stability of the fits which typically involve several
hundreds of nuisance parameters.
With that in mind, the benchmark parametrizations avoid quotients of parameters of interest.
%
Another constraint that heavily shapes the exact choice of parametrization
is consistency among the uncertainties
that can be extracted in different parametrizations.
Some coupling scale factors enter linearly in loop-induced photon and gluon vertices.
For that reason, all scale factors are defined at the same power,
leading to what could appear as an abundance of squared expressions.
%
Finally, the benchmark parametrizations are chosen such that some
potentially interesting physics scenarios can be
probed and the parameters of interest are chosen so that at least some are expected to be determined.
 
For every benchmark parametrization, two variations are provided:
\begin{enumerate}
\item The total width is scaled assuming that there are no invisible or undetected widths.
In this case $\Cc_{\PH}^2(\Cc_i,\mH)$ is a function of the free parameters.
\item The total width scale factor is
%absorbed into the parametrization.
treated as a free parameter.
In this case no assumption is done and there will be a parameter of the form $\Cc_{ij} = \Cc_i\cdot \Cc_j / \Cc_{\PH}$.
\end{enumerate}

The benchmark parametrizations are given in tabular form where each cell corresponds
to the scale factor to be applied to a given combination of production and decay mode.

For every benchmark parametrization, a list of the free parameters and their relation to the framework parameters is provided.
To reduce the amount of symbols in the tables, $\mH$ is omitted throughout.
In practice, $\mH$ can either be fixed to a given value
or profiled together with other nuisance parameters.

\subsection{One common scale factor}
\label{sec:C}

The simplest way to look for a deviation
from the predicted SM Higgs coupling structure is to leave the overall signal strength
as a free parameter.
This is presently done by the experiments,
with ATLAS finding $\mu=1.4\pm 0.3$ at 126.0\UGeV~\cite{HiggsObsATLAS}
and CMS finding $\mu=0.87\pm 0.23$ at 125.5\UGeV~\cite{HiggsObsCMS}.

In order to perform the same fit in the context of the coupling scale factor framework, the only difference is that 
$\mu = \Cc^2\cdot \Cc^2 / \Cc^2 = \Cc^2$, where the three terms $\Cc^2$ in the intermediate expression account
for production, decay and total width scaling, respectively (Table~\ref{tab:C}). 

\begin{table}[h]
\centering

\begin{tabular}{|r|c|c|c|c|c|}
\hline
\multicolumn{6}{|l|}{\bfseries Common scale factor}\\
\multicolumn{6}{|l|}{\footnotesize Free parameter: $\Cc (= \Cc_{\PQt} = \Cc_{\PQb} = \Cc_{\PGt} = \Cc_{\PW} = \Cc_{\PZ})$.} \\
\hline
 & $\PH\to\PGg\PGg$ & $\PH\to \PZ\PZ^{(*)}$ & $\PH\to \PW\PW^{(*)}$ & $\PH\to \PQb\PAQb$ & $\PH\to\PGtm\PGtp$\\
\hline
\MyggH       & \multicolumn{5}{c|}{\multirow{5}{*}{$\Cc^2$}}\\
\MyttH & \multicolumn{5}{c|}{} \\
VBF         & \multicolumn{5}{c|}{} \\
$\PW\PH$        & \multicolumn{5}{c|}{} \\
$\PZ\PH$        & \multicolumn{5}{c|}{} \\
\hline
\end{tabular}

\caption{The simplest possible benchmark parametrization where a single scale factor applies to all production and decay modes.}
\label{tab:C}
\end{table}

This parametrization, despite providing the highest experimental precision,
has several clear shortcomings,
such as ignoring that the role of the Higgs boson in providing the masses of 
the vector bosons is very different from the role it has in providing 
the masses of fermions.

\subsection{Scaling of vector boson and fermion couplings}
\label{sec:CVCF}

In checking whether an observed state is compatible with the SM Higgs boson,
one obvious question is whether it fulfills its expected role in EWSB
which is intimately related to the coupling to the vector bosons ($\PW,\PZ$).

Therefore, assuming that the SU(2) custodial symmetry holds, in the
simplest case two parameters can be defined, one
scaling the coupling to the vector bosons, $\Cc_{\PV} (=\Cc_{\PW}=\Cc_{\PZ})$,
and one scaling the coupling common to all fermions, $\Cc_{\Pf} (=\Cc_{\PQt} = \Cc_{\PQb} = \Cc_{\PGt})$.
Loop-induced processes are assumed to scale as expected from the SM structure.

In this parametrization, presented in Table~\ref{tab:CVCF},
the gluon vertex loop is effectively a fermion loop and only the photon vertex loop requires
a non-trivial scaling, given the contributions of the top and bottom quarks, of the $\PGt$ lepton, of the $\PW$-boson, as well as their
(destructive) interference.

\begin{table}[h]
\centering

\begin{tabular}{|r|c|c|c|c|c|}
\hline
\multicolumn{6}{|l|}{\bfseries Boson and fermion scaling assuming no invisible or undetectable widths}\\
\multicolumn{6}{|l|}{\footnotesize Free parameters: $\Cc_{\PV} (= \Cc_{\PW} = \Cc_{\PZ})$, $\Cc_{\Pf} (= \Cc_{\PQt} = \Cc_{\PQb} = \Cc_{\PGt})$.} \\
\hline
 & $\PH\to\PGg\PGg$ & $\PH\to \PZ\PZ^{(*)}$ & $\PH\to \PW\PW^{(*)}$ & $\PH\to \PQb\PAQb$ & $\PH\to\PGtm\PGtp$ \\
\hline
\MyggH       & \multirow{2}{*}{$\frac{\Cc_{\Pf}^2\cdot \Cc_{\PGg}^2(\Cc_{\Pf},\Cc_{\Pf},\Cc_{\Pf},\Cc_{\PV})}{\Cc_{\PH}^2(\Cc_i)}$} & \multicolumn{2}{c|}{\multirow{2}{*}{$\frac{\Cc_{\Pf}^2\cdot \Cc_{\PV}^2}{\Cc_{\PH}^2(\Cc_i)}$}} & \multicolumn{2}{c|}{\multirow{2}{*}{$\frac{\Cc_{\Pf}^2\cdot \Cc_{\Pf}^2}{\Cc_{\PH}^2(\Cc_i)}$}} \\
\MyttH &                                                                            & \multicolumn{2}{c|}{                                                 }      & \multicolumn{2}{c|}{                                                 } \\
\hline
VBF         & \multirow{3}{*}{$\frac{\Cc_{\PV}^2\cdot \Cc_{\PGg}^2(\Cc_{\Pf},\Cc_{\Pf},\Cc_{\Pf},\Cc_{\PV})}{\Cc_{\PH}^2(\Cc_i)}$} & \multicolumn{2}{c|}{\multirow{3}{*}{$\frac{\Cc_{\PV}^2\cdot \Cc_{\PV}^2}{\Cc_{\PH}^2(\Cc_i)}$}} & \multicolumn{2}{c|}{\multirow{3}{*}{$\frac{\Cc_{\PV}^2\cdot \Cc_{\Pf}^2}{\Cc_{\PH}^2(\Cc_i)}$}} \\
$\PW\PH$        &                                                                            & \multicolumn{2}{c|}{                                                 }      & \multicolumn{2}{c|}{                                                 } \\
$\PZ\PH$        &                                                                            & \multicolumn{2}{c|}{                                                 }      & \multicolumn{2}{c|}{                                                 } \\
\hline
\hline
\multicolumn{6}{|l|}{\bfseries Boson and fermion scaling without assumptions on the total width}\\
\multicolumn{6}{|l|}{\footnotesize Free parameters: $\Cc_{\PV\PV} (= \Cc_{\PV}\cdot \Cc_{\PV} / \Cc_{\PH})$, $\Rr_{\Pf\PV} (= \Cc_{\Pf} / \Cc_{\PV})$.} \\
\hline
 & $\PH\to\PGg\PGg$ & $\PH\to \PZ\PZ^{(*)}$ & $\PH\to \PW\PW^{(*)}$ & $\PH\to \PQb\PAQb$ & $\PH\to\PGtm\PGtp$ \\
\hline
\MyggH       & \multirow{2}{*}{$\Cc_{\PV\PV}^2 \cdot \Rr_{\Pf\PV}^2 \cdot \Cc_{\PGg}^2(\Rr_{\Pf\PV},\Rr_{\Pf\PV},\Rr_{\Pf\PV},1)$} & \multicolumn{2}{c|}{\multirow{2}{*}{$\Cc_{\PV\PV}^2 \cdot \Rr_{\Pf\PV}^2$}} & \multicolumn{2}{c|}{\multirow{2}{*}{$\Cc_{\PV\PV}^2 \cdot \Rr_{\Pf\PV}^2 \cdot \Rr_{\Pf\PV}^2$}} \\
\MyttH &                                      & \multicolumn{2}{c|}{                                    } & \multicolumn{2}{c|}{                                                  } \\
\hline
VBF         & \multirow{3}{*}{$\Cc_{\PV\PV}^2 \cdot \Cc_{\PGg}^2(\Rr_{\Pf\PV},\Rr_{\Pf\PV},\Rr_{\Pf\PV},1)$}  & \multicolumn{2}{c|}{\multirow{3}{*}{$\Cc_{\PV\PV}^2$}} & \multicolumn{2}{c|}{ \multirow{3}{*}{$\Cc_{\PV\PV}^2 \cdot \Rr_{\Pf\PV}^2$}} \\
$\PW\PH$        &                                      & \multicolumn{2}{c|}{                                    } & \multicolumn{2}{c|}{                                                  } \\
$\PZ\PH$        &                                      & \multicolumn{2}{c|}{                                    } & \multicolumn{2}{c|}{                                                  } \\
\hline
\end{tabular}

{\footnotesize $\Cc_i^2 = \Gamma_{ii} / \Gamma_{ii}^\text{SM}$}

\caption{A benchmark parametrization where custodial symmetry is assumed and vector boson couplings are scaled together ($\Cc_{\PV}$)
and fermions are assumed to scale with a single parameter ($\Cc_{\Pf}$).}
\label{tab:CVCF}
\end{table}

This parametrization, though exceptionally succinct, makes a number of assumptions,
which are expected to be object of further scrutiny with the accumulation of data at the LHC.
The assumptions naturally relate to the grouping of different individual couplings or to assuming that the loop amplitudes
are those predicted by the SM.

\subsection{Probing custodial symmetry}
\label{sec:RWZ}

One of the best motivated symmetries in case the new state is responsible for electroweak symmetry breaking
is the one that links its couplings to the $\PW$ and $\PZ$ bosons. 
Since $\mathrm{SU(2)_{\PV}}$ or custodial symmetry is an approximate symmetry of the SM (e.g. 
$\Delta\rho \neq 0$),
it is important to test whether data are compatible 
with the amount of violation allowed by the SM at NLO.  

In this parametrization,
presented in Table~\ref{tab:RWZ},
$\Rr_{\PW\PZ} (=\Cc_{\PW}/\Cc_{\PZ})$
%is the main parameter of interest.
is of particular interest for probing custodial symmetry.
Though providing interesting information,
both $\Cc_{\PZ}$ and $\Cc_{\Pf}$
can be thought of as nuisance parameters when performing this fit.
In addition to the photon vertex loop not having a trivial scaling,
in this parametrization also the individual $\PW$ and $\PZ$ boson fusion contributions
to the vector boson fusion production process need to be resolved.

\begin{sidewaystable}[p]
\centering
\begin{tabular}{|r|c|c|c|c|c|}
\hline
\multicolumn{6}{|l|}{\bfseries Probing custodial symmetry assuming no invisible or undetectable widths}\\
\multicolumn{6}{|l|}{\footnotesize Free parameters: $\Cc_{\PZ}$, $\Rr_{\PW\PZ} (= \Cc_{\PW} / \Cc_{\PZ})$, $\Cc_{\Pf} (= \Cc_{\PQt} = \Cc_{\PQb} = \Cc_{\PGt})$.} \\

\hline
 & $\PH\to\PGg\PGg$ & $\PH\to \PZ\PZ^{(*)}$ & $\PH\to \PW\PW^{(*)}$ & $\PH\to \PQb\PAQb$ & $\PH\to\PGtm\PGtp$ \\
\hline
\MyggH       & \multirow{2}{*}{$\frac{\Cc_{\Pf}^2\cdot \Cc_{\PGg}^2(\Cc_{\Pf},\Cc_{\Pf},\Cc_{\Pf},\Cc_{\PZ} \Rr_{\PW\PZ})}{\Cc_{\PH}^2(\Cc_i)}$} & \multirow{2}{*}{$\frac{\Cc_{\Pf}^2\cdot \Cc_{\PZ}^2}{\Cc_{\PH}^2(\Cc_i)}$} & \multirow{2}{*}{$\frac{\Cc_{\Pf}^2\cdot (\Cc_{\PZ} \Rr_{\PW\PZ})^2}{\Cc_{\PH}^2(\Cc_i)}$} & \multicolumn{2}{c|}{\multirow{2}{*}{$\frac{\Cc_{\Pf}^2\cdot \Cc_{\Pf}^2}{\Cc_{\PH}^2(\Cc_i)}$}} \\
\MyttH &                                                                & & & \multicolumn{2}{c|}{                               } \\
\hline
VBF        & $\frac{\Cc_\mathrm{VBF}^2(\Cc_{\PZ},\Cc_{\PZ} \Rr_{\PW\PZ}) \cdot \Cc_{\PGg}^2(\Cc_{\Pf},\Cc_{\Pf},\Cc_{\Pf},\Cc_{\PZ} \Rr_{\PW\PZ})}{\Cc_{\PH}^2(\Cc_i)}$ & $\frac{\Cc_\mathrm{VBF}^2(\Cc_{\PZ},\Cc_{\PZ} \Rr_{\PW\PZ})\cdot \Cc_{\PZ}^2}{\Cc_{\PH}^2(\Cc_i)}$ & $\frac{\Cc_\mathrm{VBF}^2(\Cc_{\PZ},\Cc_{\PZ} \Rr_{\PW\PZ})\cdot (\Cc_{\PZ} \Rr_{\PW\PZ})^2}{\Cc_{\PH}^2(\Cc_i)}$  & \multicolumn{2}{c|}{$\frac{\Cc_\mathrm{VBF}^2(\Cc_{\PZ},\Cc_{\PZ} \Rr_{\PW\PZ})\cdot \Cc_{\Pf}^2}{\Cc_{\PH}^2(\Cc_i)}$} \\
\hline
$\PW\PH$        & $\frac{(\Cc_{\PZ} \Rr_{\PW\PZ})^2\cdot \Cc_{\PGg}^2(\Cc_{\Pf},\Cc_{\Pf},\Cc_{\Pf},\Cc_{\PZ} \Rr_{\PW\PZ})}{\Cc_{\PH}^2(\Cc_i)}$ & $\frac{(\Cc_{\PZ} \Rr_{\PW\PZ})^2\cdot \Cc_{\PZ}^2}{\Cc_{\PH}^2(\Cc_i)}$ & $\frac{(\Cc_{\PZ} \Rr_{\PW\PZ})^2\cdot (\Cc_{\PZ} \Rr_{\PW\PZ})^2}{\Cc_{\PH}^2(\Cc_i)}$ & \multicolumn{2}{c|}{$\frac{(\Cc_{\PZ} \Rr_{\PW\PZ})^2\cdot \Cc_{\Pf}^2}{\Cc_{\PH}^2(\Cc_i)}$} \\
\hline
$\PZ\PH$        & $\frac{\Cc_{\PZ}^2\cdot \Cc_{\PGg}^2(\Cc_{\Pf},\Cc_{\Pf},\Cc_{\Pf},\Cc_{\PZ} \Rr_{\PW\PZ})}{\Cc_{\PH}^2(\Cc_i)}$          & $\frac{\Cc_{\PZ}^2\cdot \Cc_{\PZ}^2}{\Cc_{\PH}^2(\Cc_i)}$ & $\frac{\Cc_{\PZ}^2\cdot (\Cc_{\PZ} \Rr_{\PW\PZ})^2}{\Cc_{\PH}^2(\Cc_i)}$                   & \multicolumn{2}{c|}{$\frac{\Cc_{\PZ}^2\cdot \Cc_{\Pf}^2}{\Cc_{\PH}^2(\Cc_i)}$}\\
\hline
\hline
\multicolumn{6}{|l|}{\bfseries Probing custodial symmetry without assumptions on the total width}\\
\multicolumn{6}{|l|}{\footnotesize Free parameters: $\Cc_{\PZ\PZ} (= \Cc_{\PZ}\cdot \Cc_{\PZ} / \Cc_{\PH})$, $\Rr_{\PW\PZ} (= \Cc_{\PW} / \Cc_{\PZ})$, $\Rr_{FZ} (= \Cc_{\Pf} / \Cc_{\PZ})$.} \\
\hline
 & $\PH\to\PGg\PGg$ & $\PH\to \PZ\PZ^{(*)}$ & $\PH\to \PW\PW^{(*)}$ & $\PH\to \PQb\PAQb$ & $\PH\to\PGtm\PGtp$ \\
\hline
\MyggH       & \multirow{2}{*}{$\Cc_{\PZ\PZ}^2 \Rr_{FZ}^2\cdot \Cc_{\PGg}^2(\Rr_{FZ},\Rr_{FZ},\Rr_{FZ},\Rr_{\PW\PZ})$} & \multirow{2}{*}{$\Cc_{\PZ\PZ}^2 \Rr_{FZ}^2$} & \multirow{2}{*}{$\Cc_{\PZ\PZ}^2 \Rr_{FZ}^2\cdot \Rr_{\PW\PZ}^2$} & \multicolumn{2}{c|}{\multirow{2}{*}{$\Cc_{\PZ\PZ}^2 \Rr_{FZ}^2\cdot \Rr_{FZ}^2$}} \\
\MyttH &                                                                & & & \multicolumn{2}{c|}{                               } \\
\hline
VBF         & $\Cc_{\PZ\PZ}^2 \Cc_\mathrm{VBF}^2(1,\Rr_{\PW\PZ}^2)\cdot \Cc_{\PGg}^2(\Rr_{FZ},\Rr_{FZ},\Rr_{FZ},\Rr_{\PW\PZ})$   & $\Cc_{\PZ\PZ}^2 \Cc_\mathrm{VBF}^2(1,\Rr_{\PW\PZ}^2) $ & $\Cc_{\PZ\PZ}^2 \Cc_\mathrm{VBF}^2(1,\Rr_{\PW\PZ}^2)\cdot \Rr_{\PW\PZ}^2$ & \multicolumn{2}{c|}{$\Cc_{\PZ\PZ}^2 \Cc_\mathrm{VBF}^2(1,\Rr_{\PW\PZ}^2)\cdot \Rr_{FZ}^2$} \\
\hline
$\PW\PH$        & $\Cc_{\PZ\PZ}^2 \Rr_{\PW\PZ}^2\cdot \Cc_{\PGg}^2(\Rr_{FZ},\Rr_{FZ},\Rr_{FZ},\Rr_{\PW\PZ})$ & $\Cc_{\PZ\PZ}^2\cdot \Rr_{\PW\PZ}^2$ & $\Cc_{\PZ\PZ}^2 \Rr_{\PW\PZ}^2\cdot \Rr_{\PW\PZ}^2$ & \multicolumn{2}{c|}{$\Cc_{\PZ\PZ}^2 \Rr_{\PW\PZ}^2\cdot \Rr_{FZ}^2$} \\
\hline
$\PZ\PH$        & $\Cc_{\PZ\PZ}^2\cdot \Cc_{\PGg}^2(\Rr_{FZ},\Rr_{FZ},\Rr_{FZ},\Rr_{\PW\PZ})$          & $\Cc_{\PZ\PZ}^2$               & $\Cc_{\PZ\PZ}^2\cdot \Rr_{\PW\PZ}^2$          & \multicolumn{2}{c|}{$\Cc_{\PZ\PZ}^2\cdot \Rr_{FZ}^2$} \\
\hline
\end{tabular}

{\footnotesize $\Cc_i^2 = \Gamma_{ii} / \Gamma_{ii}^\text{SM}$}

\caption{A benchmark parametrization where custodial symmetry is probed through the $\Rr_{\PW\PZ}$ parameter.}
\label{tab:RWZ}
\end{sidewaystable}

\subsection{Probing the fermion sector}
\label{sec:Rdu}
\label{sec:Rlq}

In many extensions of the SM the Higgs bosons
couple differently to different types of fermions.

Given that the gluon-gluon fusion production process is dominated by the top-quark coupling,
and that there are two decay modes involving fermions,
one way of splitting fermions that is within experimental reach is
to consider up-type fermions (top quark) and
down-type fermions (bottom quark and tau lepton) separately.
%
In this parametrization, presented in Table~\ref{tab:Rdu}, the relevant parameter of interest is $\Rr_{\PQd\PQu} (=\Cc_{\PQd}/\Cc_{\PQu})$,
the ratio of the scale factors
of the couplings to down-type fermions,
$\Cc_{\PQd} = \Cc_{\PGt} (= \Cc_{\PGm}) = \Cc_{\PQb} (= \Cc_{\PQs})$, and up-type fermions, $\Cc_{\PQu} = \Cc_{\PQt} (= \Cc_{\PQc})$.

\begin{table}[p]
\centering
\begin{tabular}{|r|c|c|c|c|c|}
\hline
\multicolumn{6}{|l|}{\bfseries Probing up-type and down-type fermion symmetry assuming no invisible or undetectable widths}\\
\multicolumn{6}{|l|}{\footnotesize Free parameters: $\Cc_{\PV} (= \Cc_{\PZ} = \Cc_{\PW})$, $\Rr_{\PQd\PQu} (= \Cc_{\PQd} / \Cc_{\PQu})$, $\Cc_{\PQu} (= \Cc_{\PQt})$.} \\
\hline
 & $\PH\to\PGg\PGg$ & $\PH\to \PZ\PZ^{(*)}$ & $\PH\to \PW\PW^{(*)}$ & $\PH\to \PQb\PAQb$ & $\PH\to\PGtm\PGtp$ \\
\hline
\MyggH       & $\frac{\Cc_{\Pg}^2(\Cc_{\PQu} \Rr_{\PQd\PQu},\Cc_{\PQu})\cdot \Cc_{\PGg}^2(\Cc_{\PQu} \Rr_{\PQd\PQu},\Cc_{\PQu},\Cc_{\PQu} \Rr_{\PQd\PQu},\Cc_{\PV})}{\Cc_{\PH}^2(\Cc_i)}$ & \multicolumn{2}{c|}{$\frac{\Cc_{\Pg}^2(\Cc_{\PQu} \Rr_{\PQd\PQu},\Cc_{\PQu})\cdot \Cc_{\PV}^2}{\Cc_{\PH}^2(\Cc_i)}$} & \multicolumn{2}{c|}{$\frac{\Cc_{\Pg}^2(\Cc_{\PQu} \Rr_{\PQd\PQu},\Cc_{\PQu})\cdot (\Cc_{\PQu} \Rr_{\PQd\PQu})^2}{\Cc_{\PH}^2(\Cc_i)}$} \\
\hline
\MyttH & $\frac{\Cc_{\PQu}^2\cdot \Cc_{\PGg}^2(\Cc_{\PQu} \Rr_{\PQd\PQu},\Cc_{\PQu},\Cc_{\PQu} \Rr_{\PQd\PQu},\Cc_{\PV})}{\Cc_{\PH}^2(\Cc_i)}$                 & \multicolumn{2}{c|}{$\frac{\Cc_{\PQu}^2\cdot \Cc_{\PV}^2}{\Cc_{\PH}^2(\Cc_i)}$}                 & \multicolumn{2}{c|}{$\frac{\Cc_{\PQu}^2\cdot (\Cc_{\PQu} \Rr_{\PQd\PQu})^2}{\Cc_{\PH}^2(\Cc_i)}$}  \\
\hline
VBF         & \multirow{3}{*}{$\frac{\Cc_{\PV}^2\cdot \Cc_{\PGg}^2(\Cc_{\PQu} \Rr_{\PQd\PQu},\Cc_{\PQu},\Cc_{\PQu} \Rr_{\PQd\PQu},\Cc_{\PV})}{\Cc_{\PH}^2(\Cc_i)}$} & \multicolumn{2}{c|}{\multirow{3}{*}{$\frac{\Cc_{\PV}^2\cdot \Cc_{\PV}^2}{\Cc_{\PH}^2(\Cc_i)}$}} & \multicolumn{2}{c|}{\multirow{3}{*}{$\frac{\Cc_{\PV}^2\cdot (\Cc_{\PQu} \Rr_{\PQd\PQu})^2}{\Cc_{\PH}^2(\Cc_i)}$}} \\
$\PW\PH$        &                                                                              & \multicolumn{2}{c|}{                                                 } & \multicolumn{2}{c|}{                                          } \\
$\PZ\PH$        &                                                                              & \multicolumn{2}{c|}{                                                 } & \multicolumn{2}{c|}{                                          } \\
\hline
\hline
\multicolumn{6}{|l|}{\bfseries Probing up-type and down-type fermion symmetry without assumptions on the total width}\\
\multicolumn{6}{|l|}{\footnotesize Free parameters: $\Cc_{\PQu\PQu} (= \Cc_{\PQu}\cdot \Cc_{\PQu} / \Cc_{\PH})$, $\Rr_{\PQd\PQu} (= \Cc_{\PQd} / \Cc_{\PQu})$, $\Rr_{\PV\PQu} (= \Cc_{\PV} / \Cc_{\PQu})$.} \\
\hline
 & $\PH\to\PGg\PGg$ & $\PH\to \PZ\PZ^{(*)}$ & $\PH\to \PW\PW^{(*)}$ & $\PH\to \PQb\PAQb$ & $\PH\to\PGtm\PGtp$ \\
\hline
\MyggH       & $\Cc_{\PQu\PQu}^2 \Cc_{\Pg}^2(\Rr_{\PQd\PQu}, 1)\cdot \Cc_{\PGg}^2(\Rr_{\PQd\PQu}, 1,\Rr_{\PQd\PQu}, \Rr_{\PV\PQu})$ & \multicolumn{2}{c|}{$\Cc_{\PQu\PQu}^2 \Cc_{\Pg}^2(\Rr_{\PQd\PQu}, 1)\cdot \Rr_{\PV\PQu}^2$} & \multicolumn{2}{c|}{$\Cc_{\PQu\PQu}^2 \Cc_{\Pg}^2(\Rr_{\PQd\PQu}, 1)\cdot \Rr_{\PQd\PQu}^2$} \\
\hline
\MyttH & $\Cc_{\PQu\PQu}^2\cdot \Cc_{\PGg}^2(\Rr_{\PQd\PQu}, 1,\Rr_{\PQd\PQu}, \Rr_{\PV\PQu})$                  & \multicolumn{2}{c|}{$\Cc_{\PQu\PQu}^2\cdot \Rr_{\PV\PQu}^2$}                  & \multicolumn{2}{c|}{$\Cc_{\PQu\PQu}^2\cdot \Rr_{\PQd\PQu}^2$} \\
\hline
VBF         & \multirow{3}{*}{$\Cc_{\PQu\PQu}^2 \Rr_{\PV\PQu}^2\cdot \Cc_{\PGg}^2(\Rr_{\PQd\PQu}, 1,\Rr_{\PQd\PQu}, \Rr_{\PV\PQu})$} & \multicolumn{2}{c|}{\multirow{3}{*}{$\Cc_{\PQu\PQu}^2 \Rr_{\PV\PQu}^2\cdot \Rr_{\PV\PQu}^2$}} & \multicolumn{2}{c|}{\multirow{3}{*}{$\Cc_{\PQu\PQu}^2 \Rr_{\PV\PQu}^2\cdot \Rr_{\PQd\PQu}^2$}} \\
$\PW\PH$        &                                                                           & \multicolumn{2}{c|}{                                                  } & \multicolumn{2}{c|}{                                          } \\
$\PZ\PH$        &                                                                           & \multicolumn{2}{c|}{                                                  } & \multicolumn{2}{c|}{                                          } \\
\hline
\end{tabular}

{\footnotesize $\Cc_i^2 = \Gamma_{ii} / \Gamma_{ii}^\text{SM}$, $\Cc_{\PQd} = \Cc_{\PQb} = \Cc_{\PGt}$}
\caption{A benchmark parametrization where the up-type and down-type symmetry of fermions is probed through the $\Rr_{\PQd\PQu}$ parameter.}
\label{tab:Rdu}
\end{table}

Alternatively one can consider quarks and leptons separately.
%
In this parametrization, presented in Table~\ref{tab:Rlq},
the relevant parameter of interest is $\Rr_{\Pl\PQq} (=\Cc_{\Pl}/\Cc_{\PQq})$,
the ratio of the coupling scale factors to leptons,
$\Cc_{\Pl} = \Cc_{\PGt} (= \Cc_{\PGm})$, and quarks, $\Cc_{\PQq} = \Cc_{\PQt} (= \Cc_{\PQc}) = \Cc_{\PQb} (= \Cc_{\PQs})$.

\begin{table}[p]
\centering
\begin{tabular}{|r|c|c|c|c|c|}
\hline
\multicolumn{6}{|l|}{\bfseries Probing quark and lepton fermion symmetry assuming no invisible or undetectable widths}\\
\multicolumn{6}{|l|}{\footnotesize Free parameters: $\Cc_{\PV} (= \Cc_{\PZ} = \Cc_{\PW})$, $\Rr_{\Pl\PQq} (= \Cc_{\Pl} / \Cc_{\PQq})$, $\Cc_{\PQq} (= \Cc_{\PQt} = \Cc_{\PQb})$.} \\
\hline
 & $\PH\to\PGg\PGg$ & $\PH\to \PZ\PZ^{(*)}$ & $\PH\to \PW\PW^{(*)}$ & $\PH\to \PQb\PAQb$ & $\PH\to\PGtm\PGtp$ \\
\hline
\MyggH       & \multirow{2}{*}{$\frac{\Cc_{\PQq}^2\cdot \Cc_{\PGg}^2(\Cc_{\PQq},\Cc_{\PQq},\Cc_{\PQq} \Rr_{\Pl\PQq},\Cc_{\PV})}{\Cc_{\PH}^2(\Cc_i)}$} & \multicolumn{2}{c|}{\multirow{2}{*}{$\frac{\Cc_{\PQq}^2\cdot \Cc_{\PV}^2}{\Cc_{\PH}^2(\Cc_i)}$}} & \multirow{2}{*}{$\frac{\Cc_{\PQq}^2\cdot \Cc_{\PQq}^2}{\Cc_{\PH}^2(\Cc_i)}$} & \multirow{2}{*}{$\frac{\Cc_{\PQq}^2\cdot (\Cc_{\PQq} \Rr_{\Pl\PQq})^2}{\Cc_{\PH}^2(\Cc_i)}$} \\
\MyttH &                                                                       & \multicolumn{2}{c|}{                                 }                 & &  \\
\hline
VBF         & \multirow{3}{*}{$\frac{\Cc_{\PV}^2\cdot \Cc_{\PGg}^2(\Cc_{\PQq},\Cc_{\PQq},\Cc_{\PQq} \Rr_{\Pl\PQq},\Cc_{\PV})}{\Cc_{\PH}^2(\Cc_i)}$} & \multicolumn{2}{c|}{\multirow{3}{*}{$\frac{\Cc_{\PV}^2\cdot \Cc_{\PV}^2}{\Cc_{\PH}^2(\Cc_i)}$}} & \multirow{3}{*}{$\frac{\Cc_{\PV}^2\cdot \Cc_{\PQq}^2}{\Cc_{\PH}^2(\Cc_i)}$} & \multirow{3}{*}{$\frac{\Cc_{\PV}^2\cdot (\Cc_{\PQq} \Rr_{\Pl\PQq})^2}{\Cc_{\PH}^2(\Cc_i)}$} \\
$\PW\PH$        &                                                                       & \multicolumn{2}{c|}{                                                 } & & \\
$\PZ\PH$        &                                                                       & \multicolumn{2}{c|}{                                                 } & & \\
\hline
\hline
\multicolumn{6}{|l|}{\bfseries Probing quark and lepton fermion symmetry without assumptions on the total width}\\
\multicolumn{6}{|l|}{\footnotesize Free parameters: $\Cc_{\PQq\PQq} (= \Cc_{\PQq}\cdot \Cc_{\PQq} / \Cc_{\PH})$, $\Rr_{\Pl\PQq} (= \Cc_{\Pl} / \Cc_{\PQq})$, $\Rr_{\PV\PQq} (= \Cc_{\PV} / \Cc_{\PQq})$.} \\
\hline
 & $\PH\to\PGg\PGg$ & $\PH\to \PZ\PZ^{(*)}$ & $\PH\to \PW\PW^{(*)}$ & $\PH\to \PQb\PAQb$ & $\PH\to\PGtm\PGtp$ \\
\hline
\MyggH       & \multirow{2}{*}{$\Cc_{\PQq\PQq}^2\cdot \Cc_{\PGg}^2(1,1,\Rr_{\Pl\PQq},\Rr_{\PV\PQq})$} & \multicolumn{2}{c|}{\multirow{2}{*}{$\Cc_{\PQq\PQq}^2\cdot \Rr_{\PV\PQq}^2$}} & \multirow{2}{*}{$\Cc_{\PQq\PQq}^2$} & \multirow{2}{*}{$\Cc_{\PQq\PQq}^2\cdot \Rr_{\Pl\PQq}^2$} \\
\MyttH &                                                                       & \multicolumn{2}{c|}{                                 }                 & &  \\
\hline
VBF         & \multirow{3}{*}{$\Cc_{\PQq\PQq}^2 \Rr_{\PV\PQq}^2\cdot \Cc_{\PGg}^2(1,1,\Rr_{\Pl\PQq},\Rr_{\PV\PQq})$} & \multicolumn{2}{c|}{\multirow{3}{*}{$\Cc_{\PQq\PQq}^2 \Rr_{\PV\PQq}^2\cdot \Rr_{\PV\PQq}^2$}} & \multirow{3}{*}{$\Cc_{\PQq\PQq}^2\cdot \Rr_{\PV\PQq}^2$} & \multirow{3}{*}{$\Cc_{\PQq\PQq}^2 \Rr_{\PV\PQq}^2\cdot \Rr_{\Pl\PQq}^2$} \\
$\PW\PH$        &                                                                       & \multicolumn{2}{c|}{                                                 } & & \\
$\PZ\PH$        &                                                                       & \multicolumn{2}{c|}{                                                 } & & \\
\hline
\end{tabular}

{\footnotesize $\Cc_i^2 = \Gamma_{ii} / \Gamma_{ii}^\text{SM}$, $\Cc_{\Pl} = \Cc_{\PGt}$}

\caption{A benchmark parametrization where the quark and lepton symmetry of fermions is probed through the $\Rr_{\Pl\PQq}$ parameter.}
\label{tab:Rlq}
\end{table}

One further combination of top-quark, bottom-quark and tau-lepton, namely
scaling the top-quark and tau-lepton with a common parameter and the
bottom-quark with another parameter, can be envisaged and readily
parametrized based on the interim framework but is not put forward as a benchmark.

\subsection{Probing the loop structure and invisible or undetectable decays}
\label{sec:CgCgam}

New particles associated with physics beyond the SM
may influence the partial width of the gluon and/or photon vertices.

In this parametrization, presented in Table~\ref{tab:CgCgam},
each of the loop-induced vertices is represented by an effective scale factor, $\Cc_{\Pg}$ and $\Cc_{\PGg}$.

Particles not predicted by the SM may also give rise to invisible or undetectable
decays.
Invisible decays might show up as a MET signature and could potentially be measured at the LHC with dedicated analyses. 
An example of an undetectable final state would be a multi-jet signature that
cannot be separated from QCD backgrounds at the LHC and hence not detected.
With sufficient data it can be envisaged to disentangle the invisible and undetectable components.

In order to probe this possibility, instead of absorbing the total width into another parameter or leaving it free,
a different parameter is introduced, $\BRinv$.
The definition of $\BRinv$ is relative to the rescaled total width, $\Cc_{\PH}^2(\Cc_i)$,
and can thus be interpreted as the invisible or undetectable fraction of the total width. 

\begin{table}[p]
\centering
\begin{tabular}{|r|c|c|c|c|c|}
\hline
\multicolumn{6}{|l|}{\bfseries Probing loop structure assuming no invisible or undetectable widths}\\
\multicolumn{6}{|l|}{\footnotesize Free parameters: $\Cc_{\Pg}$, $\Cc_{\PGg}$.} \\
\hline
 & $\PH\to\PGg\PGg$ & $\PH\to \PZ\PZ^{(*)}$ & $\PH\to \PW\PW^{(*)}$ & $\PH\to \PQb\PAQb$ & $\PH\to\PGtm\PGtp$  \\
\hline
\MyggH       & $\frac{\Cc_{\Pg}^2\cdot \Cc_{\PGg}^2}{\Cc_{\PH}^2(\Cc_i)}$ & \multicolumn{4}{c|}{$\frac{\Cc_{\Pg}^2}{\Cc_{\PH}^2(\Cc_i)}$} \\
\hline
\MyttH & \multirow{4}{*}{$\frac{\Cc_{\PGg}^2}{\Cc_{\PH}^2(\Cc_i)}$}& \multicolumn{4}{c|}{\multirow{4}{*}{$\frac{1}{\Cc_{\PH}^2(\Cc_i)}$}    } \\
VBF         &                                                   & \multicolumn{4}{c|}{                           } \\
$\PW\PH$        &                                                   & \multicolumn{4}{c|}{                           } \\
$\PZ\PH$        &                                                   & \multicolumn{4}{c|}{                           } \\
\hline
\hline
\multicolumn{6}{|l|}{\bfseries Probing loop structure allowing for invisible or undetectable widths}\\
\multicolumn{6}{|l|}{\footnotesize Free parameters: $\Cc_{\Pg}$, $\Cc_{\PGg}$, $\BRinv$.} \\
\hline
 & $\PH\to\PGg\PGg$ & $\PH\to \PZ\PZ^{(*)}$ & $\PH\to \PW\PW^{(*)}$ & $\PH\to \PQb\PAQb$ & $\PH\to\PGtm\PGtp$  \\
\hline
\MyggH       & $\frac{\Cc_{\Pg}^2\cdot \Cc_{\PGg}^2}{\Cc_{\PH}^2(\Cc_i) /(1-\BRinv)}$ & \multicolumn{4}{c|}{$\frac{\Cc_{\Pg}^2}{\Cc_{\PH}^2(\Cc_i) /(1-\BRinv)}$} \\
\hline
\MyttH & \multirow{4}{*}{$\frac{\Cc_{\PGg}^2}{\Cc_{\PH}^2(\Cc_i) /(1-\BRinv)}$}& \multicolumn{4}{c|}{\multirow{4}{*}{$\frac{1}{\Cc_{\PH}^2(\Cc_i) /(1-\BRinv)}$}    } \\
VBF         &                                                   & \multicolumn{4}{c|}{                           } \\
$\PW\PH$        &                                                   & \multicolumn{4}{c|}{                           } \\
$\PZ\PH$        &                                                   & \multicolumn{4}{c|}{                           } \\
\hline
\end{tabular}

{\footnotesize $\Cc_i^2 = \Gamma_{ii} / \Gamma_{ii}^\text{SM}$}

\caption{A benchmark parametrization where effective vertex couplings are allowed to float through the $\Cc_{\Pg}$ and $\Cc_{\PGg}$ parameters.
Instead of absorbing $\Cc_{\PH}$, explicit allowance is made for a contribution from invisible or undetectable widths via the $\BRinv$ parameter.}
\label{tab:CgCgam}
\end{table}

One particularity of this benchmark parametrization is that it should allow
%any
theoretical predictions involving new particles to be projected into the
 $(\Cc_{\Pg},\Cc_{\PGg})$ or $(\Cc_{\Pg},\Cc_{\PGg},\BRinv)$ spaces.

It can be noted that the benchmark parametrization including
\BRinv\ can be recast in a form that allows
for an interpretation in terms of a tree-level scale factor and
the loop-induced scale factors with the following
substitutions: $\Cc_j \to \Cc_j^\prime / \Cc_\mathrm{tree}$
(with $j=\Pg,\PGg$) and $(1-\BRinv) \to \Cc_\mathrm{tree}^2$.


\subsection{A minimal parametrization without assumptions on new physics contributions}

Finally, the following parametrization gathers the most
important degrees of freedom considered before,
namely $\Cc_{\Pg}$, $\Cc_{\PGg}$, $\Cc_{\PV}$, $\Cc_{\Pf}$.
The parametrization, presented in Table~\ref{tab:minimal_noassumptions},
 is chosen such that some parameters are expected to be reasonably constrained by the LHC data in the near term,
while other parameters are not expected to be as well constrained in the same time frame. 

It should be noted that this is a parametrization which only includes trivial scale factors.

With the presently available analyses and data, $\Cc_{\Pg \PV}^2 = \Cc_{\Pg}^2\cdot \Cc_{\PV}^2 / \Cc_{\PH}^2$
seems to be a good choice for the common $\Cc_{ij}$ parameter.

\begin{table}[p]
\centering
\begin{tabular}{|r|c|c|c|c|c|}
\hline
\multicolumn{6}{|l|}{\bfseries Probing loops while allowing other couplings to float assuming no invisible or undetectable widths} \\
\multicolumn{6}{|l|}{\footnotesize Free parameters: $\Cc_{\Pg}$, $\Cc_{\PGg}$, $\Cc_{\PV} (=\Cc_{\PW}=\Cc_{\PZ})$, $\Cc_{\Pf} (=\Cc_{\PQt}=\Cc_{\PQb}=\Cc_{\PGt})$.} \\
\hline
 & $\PH\to\PGg\PGg$ & $\PH\to \PZ\PZ^{(*)}$ & $\PH\to \PW\PW^{(*)}$ & $\PH\to \PQb\PAQb$ & $\PH\to\PGtm\PGtp$ \\
\hline
\MyggH       & $\frac{\Cc_{\Pg}^2\cdot \Cc_{\PGg}^2}{\Cc_{\PH}^2(\Cc_i)}$ & \multicolumn{2}{c|}{$\frac{\Cc_{\Pg}^2\cdot \Cc_{\PV}^2}{\Cc_{\PH}^2(\Cc_i)}$} & \multicolumn{2}{c|}{$\frac{\Cc_{\Pg}^2\cdot \Cc_{\Pf}^2}{\Cc_{\PH}^2(\Cc_i)}$} \\
\hline
\MyttH & $\frac{\Cc_{\Pf}^2\cdot \Cc_{\PGg}^2}{\Cc_{\PH}^2(\Cc_i)}$ & \multicolumn{2}{c|}{$\frac{\Cc_{\Pf}^2\cdot \Cc_{\PV}^2}{\Cc_{\PH}^2(\Cc_i)}$} & \multicolumn{2}{c|}{$\frac{\Cc_{\Pf}^2\cdot \Cc_{\Pf}^2}{\Cc_{\PH}^2(\Cc_i)}$} \\
\hline
VBF         & \multirow{3}{*}{$\frac{\Cc_{\PV}^2\cdot \Cc_{\PGg}^2}{\Cc_{\PH}^2(\Cc_i)}$} & \multicolumn{2}{c|}{\multirow{3}{*}{$\frac{\Cc_{\PV}^2\cdot \Cc_{\PV}^2}{\Cc_{\PH}^2(\Cc_i)}$}} & \multicolumn{2}{c|}{\multirow{3}{*}{$\frac{\Cc_{\PV}^2\cdot \Cc_{\Pf}^2}{\Cc_{\PH}^2(\Cc_i)}$}} \\
$\PW\PH$        &                                                          & \multicolumn{2}{c|}{                                                 } & \multicolumn{2}{c|}{                                                 } \\
$\PZ\PH$        &                                                          & \multicolumn{2}{c|}{                                                 } & \multicolumn{2}{c|}{                                                 } \\
\hline
\hline
\multicolumn{6}{|l|}{\bfseries Probing loops while allowing other couplings to float allowing for invisible or undetectable widths} \\
\multicolumn{6}{|l|}{\footnotesize Free parameters: $\Cc_{\Pg\PV} (= \Cc_{\Pg}\cdot \Cc_{\PV} / \Cc_{\PH})$, $\Rr_{\PV\Pg}(=\Cc_{\PV}/\Cc_{\Pg})$, $\Rr_{\PGg\PV}(=\Cc_{\PGg} /\Cc_{\PV})$, $\Rr_{\Pf\PV}(=\Cc_{\Pf}/\Cc_{\PV})$.} \\
\hline
 & $\PH\to\PGg\PGg$ & $\PH\to \PZ\PZ^{(*)}$ & $\PH\to \PW\PW^{(*)}$ & $\PH\to \PQb\PAQb$ & $\PH\to\PGtm\PGtp$ \\
\hline
\MyggH       & $\Cc_{\Pg\PV}^2\cdot \Rr_{\PGg\PV}^2$                   & \multicolumn{2}{c|}{$\Cc_{\Pg\PV}^2$}                   & \multicolumn{2}{c|}{$\Cc_{\Pg\PV}^2\cdot \Rr_{\Pf\PV}^2$} \\
\hline
\MyttH & $\Cc_{\Pg\PV}^2 \Rr_{\PV\Pg}^2 \Rr_{\Pf\PV}^2\cdot \Rr_{\PGg\PV}^2$ & \multicolumn{2}{c|}{$\Cc_{\Pg\PV}^2 \Rr_{\PV\Pg}^2 \Rr_{\Pf\PV}^2$} & \multicolumn{2}{c|}{$\Cc_{\Pg\PV}^2 \Rr_{\PV\Pg}^2 \Rr_{\Pf\PV}^2\cdot \Rr_{\Pf\PV}^2$} \\
\hline
VBF         & \multirow{3}{*}{$\Cc_{\Pg\PV}^2 \Rr_{\PV\Pg}^2\cdot \Rr_{\PGg\PV}^2$} & \multicolumn{2}{c|}{\multirow{3}{*}{$\Cc_{\Pg\PV}^2 \Rr_{\PV\Pg}^2$}} & \multicolumn{2}{c|}{\multirow{3}{*}{$\Cc_{\Pg\PV}^2 \Rr_{\PV\Pg}^2\cdot \Rr_{\Pf\PV}^2$}} \\
$\PW\PH$        &                                                          & \multicolumn{2}{c|}{                                    } & \multicolumn{2}{c|}{                                          } \\
$\PZ\PH$        &                                                          & \multicolumn{2}{c|}{                                    } & \multicolumn{2}{c|}{                                          } \\
\hline
\end{tabular}

{\footnotesize $\Cc_i^2 = \Gamma_{ii} / \Gamma_{ii}^\text{SM}$, $\Cc_{\PV} = \Cc_{\PW} = \Cc_{\PZ}$, $\Cc_{\Pf} = \Cc_{\PQt} = \Cc_{\PQb} = \Cc_{\PGt}$}

\caption{A benchmark parametrization where effective vertex couplings are allowed to float through the $\Cc_{\Pg}$ and $\Cc_{\PGg}$ parameters
and the gauge and fermion couplings through the unified parameters $\Cc_{\PV}$ and $\Cc_{\Pf}$.}
\label{tab:minimal_noassumptions}
\end{table}

