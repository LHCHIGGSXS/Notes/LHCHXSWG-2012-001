\section{Introduction}

The recent observation of a new massive neutral boson by ATLAS and CMS~\cite{HiggsObsATLAS, HiggsObsCMS},
as well as
evidence from the Tevatron experiments~\cite{HiggsObsTevatron}, opens a new era where characterization
of this new object is of central importance.

The Standard Model (SM), as any renormalizable theory,
makes very accurate predictions for the coupling of the Higgs boson
to all other known particles.
These couplings directly influence the rates of production and decay of the Higgs boson.
Therefore, measurement of the production and decay rates of the observed state yields information
that can be used to probe whether data are compatible with the SM predictions for the Higgs boson.

While coarse features of the observed state can be inferred from the information that the experiments have made public,
only a consistent and combined treatment of the data can yield the most accurate picture of the coupling structure.
Such a treatment must take into account all the systematic and statistical uncertainties considered in the analyses,
as well as the correlations among them. 

This document outlines an interim framework to explore the coupling structure of the recently
observed state.
The framework proposed in this recommendation should be seen as a continuation of the earlier studies of the LHC sensitivity to the Higgs couplings initiated in Refs.~\cite{Zeppenfeld:2000td, Duhrssen:2003aa, Duhrssen:2004cv, Lafaye:2009vr}, and has been influenced by the works of Refs.~\cite{Hagiwara:1993qt, GonzalezGarcia:1999fq, Barger:2003rs, Manohar:2006gz, Giudice:2007fh, Low:2009di, Espinosa:2010vn, Anastasiou:2011pi}.  It follows closely the methodology proposed in the recent phenomenological works of Refs.~\cite{Carmi:2012yp, Azatov:2012bz, Espinosa:2012ir} which have been further extended in several directions \cite{Cao:2012fz, Boudjema:2012cq, Barger:2012hv, Frandsen:2012rj, Giardino:2012ww, Ellis:2012rx, Draper:2012xt, Azatov:2012rd, Farina:2012ea, Englert:2012cb, Degrande:2012gr, Klute:2012pu, Arhrib:2012yv, Akeroyd:2012ms, Azatov:2012wq, Carena:2012xa, Gupta:2012mi, Blum:2012ii, Chang:2012tb, Chang:2012gn, Low:2012rj, Benbrik:2012rm, Corbett:2012dm, Giardino:2012dp, Ellis:2012hz, Montull:2012ik, Espinosa:2012im, Carmi:2012in, Peskin:2012we, Banerjee:2012xc, Cao:2012yn, Bertolini:2012gu, ArkaniHamed:2012kq, Bonnet:2012nm, Craig:2012vn, Almeida:2012bq, Alves:2012ez, Espinosa:2012in, Accomando:2012yg, Elander:2012fk, Reece:2012gi, Djouadi:2012rh} along the lines that are formalized in the present recommendation.
While the interim framework is not final,
it has an accuracy that matches the statistical power of the datasets that the LHC experiments
can hope to collect until the end of the 2012 LHC run
and is an explicit attempt to provide a common ground
for the dialogue in the, and between the, experimental and theoretical communities. 

Based on that framework, a series of benchmark parametrizations are presented.
Each benchmark parametrization allows to explore specific aspects of the coupling structure of the new state.
The parametrizations have varying degrees of complexity,
with the aim to cover the most interesting possibilities that can be realistically tested with
the LHC 7 and 8\UTeV\ datasets.
On the one hand, the framework and benchmarks were designed to provide a recommendation
to experiments on how to perform coupling fits that are useful for the theory community.
On the other hand the theory community can prepare for results based on the framework discussed in this document.

%The recommended benchmarks in this document were designed to provide a solid basis on which two aspects can be developed.
%On the one hand, the recommendation can be used as basis by the experiments to combine their results according to clearly defined parametrizations.
%On the other hand, theoretical work can be developed with knowledge of which parametrizations experiments will
%use to publish their results. 

%Finally, avenues that can be followed to improve upon this interim framework are briefly expounded upon.
Finally, avenues that can be pursued to improve upon this interim framework and
recommendations on how to probe the tensor structure will be discussed in a future document.

\section{Panorama of experimental measurements at the LHC}

In 2011, the LHC delivered an integrated luminosity of slightly less
than 6\ifb\ of proton--proton ($\Pp\Pp$) collisions at a center-of-mass
energy of 7\UTeV\ to the ATLAS and CMS experiments. 
%
By July 2012, the LHC delivered more than 6\ifb\ of $\Pp\Pp$ collisions
at a center-of-mass energy of 8\UTeV\ to both experiments. 
For this dataset, the instantaneous luminosity reached
record levels of approximately $7 \cdot 10^{33}$~cm$^{-2}$s$^{-1}$,
almost double the peak luminosity of 2011 with the same 50~ns bunch
spacing.
The 2012 $\Pp\Pp$ run will continue until the end of
the year, hopefully delivering about 30\ifb\ per
experiment.
 
At the LHC a SM-like Higgs boson is searched for mainly in four
exclusive production processes:
the predominant gluon fusion \ggF,
the vector boson fusion \VBF,
the associated production with a vector boson \VH\ and
the associated production with a top-quark pair \ttH.
The main search
channels are determined by five decay modes of the Higgs boson, the
$\PGg\PGg$, $\PZ\PZ^{(*)}$, $\PW\PW^{(*)}$, $\PQb\PAQb$ and
$\PGtp\PGtm$ channels.
The mass range within which each channel is effective
and the production processes for which exclusive searches
have been developed and made public
are indicated in Table~\ref{tab:channels}.
A detailed description of the Higgs search analyses
can be found in~\Brefs{HiggsObsATLAS,HiggsObsCMS}. 

\begin{table}[htb]
\centering
  \caption{Summary of the Higgs boson search channels in
    the ATLAS and CMS experiments by July 2012.
    The $\surd$ symbol indicates exclusive searches targetting the inclusive \ggF\ production, 
    the associated production processes (with a vector boson or a top quark pair)
    or the vector boson fusion (VBF) production process.}
\label{tab:channels}
\begin{tabular}{lc|cc|cc|cc|cc}\hline
  Channel & $\mH (\UGeV)$ &  \multicolumn{2}{c}{\MyggH} &
  \multicolumn{2}{c}{VBF} & \multicolumn{2}{c}{VH} &
  \multicolumn{2}{c}{\MyttH} \\ \hline
%  & & \tiny ATLAS & \tiny CMS  & \tiny ATLAS & \tiny CMS  & \tiny ATLAS & \tiny CMS  & \tiny ATLAS & \tiny CMS   \\ \hline
  & & \scriptsize ATLAS & \scriptsize CMS  & \scriptsize ATLAS & \scriptsize CMS  & \scriptsize ATLAS & \scriptsize CMS  & \scriptsize ATLAS & \scriptsize CMS   \\ \hline
  \hgg\     & 110--150 & $\surd$ & $\surd$ & $\surd$ & $\surd$  & - & -  & - & -       \\ \hline
  \htt\     & 110--145 & $\surd$ & $\surd$ & $\surd$ & $\surd$  & $\surd$ & $\surd$ & - & -       \\ \hline
  \hbb\     & 110--130 & - & - & - & -  & $\surd$ & $\surd$ & - & $\surd$ \\ \hline
  \hZZllll\ & 110--600 & $\surd$ & $\surd$ & - & -  & - & -  & - & -       \\ \hline
  \hWWlnln\ & 110--600 & $\surd$ & $\surd$ & $\surd$ & $\surd$  & $\surd$ & $\surd$ & - & -       \\ \hline
\end{tabular}
\end{table}

Both the ATLAS and CMS experiments observe an excess of events for
Higgs boson mass hypotheses near $\sim 125\UGeV$.
The observed combined significances are $5.9\sigma$ for ATLAS~\cite{HiggsObsATLAS}
and 5.0$\sigma$ for CMS~\cite{HiggsObsCMS}, compatible with their respective sensitivities.
Both observations are primarily in the \hgg, \hZZllll\ and \hWWlnln\ channels.
For the \hgg\ channel, excesses of $4.5\sigma$ and $4.1\sigma$ are observed
at Higgs boson mass hypotheses of $126.5\UGeV$ and $125\UGeV$,
in agreement with the expected sensitivities of around $2.5\sigma$ and $2.8\sigma$,
in the ATLAS and CMS experiments respectively.
For the \hZZllll\ channel, the significances of the excesses are 3.6$\sigma$ and 3.2$\sigma$
at Higgs boson mass hypotheses of $125\UGeV$ and $125.6\UGeV$,
in the ATLAS and CMS experiments respectively.
The expected sensitivities at those masses are 2.7$\sigma$ in ATLAS and 3.8$\sigma$
in CMS respectively.
For the low mass resolution \hWWlnln\ channel ATLAS observes an excess of 2.8$\sigma$ (2.3$\sigma$ expected) 
and CMS observes 1.6$\sigma$ (2.4$\sigma$ expected) for a Higgs boson mass hypotheses of $\sim 125\UGeV$.
The other channels do not contribute
significantly to the excess, but are nevertheless individually
compatible with the presence of a signal.

The ATLAS and CMS experiments have also reported compatible
measurements of the mass of the observed narrow resonance yielding:
\begin{center}
  126.0~$\pm 0.4$(stat.)~$\pm 0.4$(syst.)\UGeV (ATLAS), \\
  125.3~$\pm 0.4$(stat.)~$\pm 0.5$(syst.)\UGeV (CMS).
\end{center}
